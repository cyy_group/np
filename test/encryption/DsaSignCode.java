package encryption;

import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.security.spec.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
// Copyright (C) 2003  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license/GPL-2.0
public class DsaSignCode {

    private static final String SIGNATURE_ALGORITHM = "SHA1withDSA";
    private static final String ALGORITHM = "DSA";
    private static final int KEY_SIZE = 1024;

    //    该方法取一个随机x，x小于q并且大于0，
    private static BigInteger randbint(BigInteger n) {
        Random rnd = new Random();
        int maxNumBitLength = n.bitLength();
        BigInteger aRandomBigInt;
        do {
            aRandomBigInt = new BigInteger(maxNumBitLength, rnd);
        } while (aRandomBigInt.compareTo(n) > 0);
        return aRandomBigInt;
    }

    public Map<String, BigInteger> DsaKey() throws NoSuchAlgorithmException {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
        //初始化
        keyGen.initialize(KEY_SIZE);
        KeyPair keyPair = keyGen.genKeyPair();
        //使用KeyPair生成密钥
        DSAPrivateKey privateKey = (DSAPrivateKey) keyPair.getPrivate();
//        DSAPublicKey publicKey = (DSAPublicKey) keyPair.getPublic();
        DSAParams dsaParams = privateKey.getParams();
        BigInteger p = dsaParams.getP();
        BigInteger q = dsaParams.getQ();
        BigInteger g = dsaParams.getG();
        BigInteger x = randbint(q);
        BigInteger y = g.modPow(x, p);
        //使用HASHMAP存储p,q,g,x,y
        Map<String, BigInteger> map = new HashMap<String, BigInteger>(5);
        map.put("KEY_P", p);
        map.put("KEY_Q", q);
        map.put("KEY_G", g);
        map.put("KEY_X", x);
        map.put("KEY_Y", y);
        return map;
    }

    //得到标准的公钥
    public PublicKey getPublicKey(BigInteger y, BigInteger p, BigInteger q, BigInteger g) throws NoSuchAlgorithmException, InvalidKeySpecException {

        DSAPublicKeySpec dsaPublicKeySpec = new DSAPublicKeySpec(y, p, q, g);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        //根据提供的密钥规范（密钥材料）生成公钥对象
        return keyFactory.generatePublic(dsaPublicKeySpec);
    }

    //得到标准的私钥
    public PrivateKey getPrivateKey(BigInteger x, BigInteger p, BigInteger q, BigInteger g) throws NoSuchAlgorithmException, InvalidKeySpecException {

        DSAPrivateKeySpec dsaPrivateKeySpec = new DSAPrivateKeySpec(x, p, q, g);
        PrivateKey privateKey = null;
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        //根据提供的密钥规范（密钥材料）生成私钥对象
        privateKey = keyFactory.generatePrivate(dsaPrivateKeySpec);
        return privateKey;
    }
}