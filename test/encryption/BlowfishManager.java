package encryption;
// Copyright (C) 2003  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license/GPL-2.0
public enum BlowfishManager {

    BRIDGELI_CN("bridgeli_cn!@#$abc123_");

    private BlowfishManager(String secret) {
        this.blowfish = new Blowfish(secret);
    }

    private Blowfish blowfish;

    public Blowfish getBlowfish() {
        return blowfish;
    }

    /**
     * 解密
     * @param sCipherText
     * @return
     */
    public String decryptString(String sCipherText){
        return this.getBlowfish().decryptString(sCipherText);
    }

    /**
     * 加密
     * @param sPlainText
     * @return
     */
    public String encryptString(String sPlainText){
        return this.getBlowfish().encryptString(sPlainText);
    }


}